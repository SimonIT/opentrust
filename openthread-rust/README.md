# openthread-rust

Minimal sample for the usage of [openthread-sys](https://gitlab.com/markushx/opentrust/-/tree/master/openthread-sys).

## Status

Work in Progress

### Features

- [x] Platform Adaptation
  - [x] Minimal Sample
  - [ ] Functional, e.g. based on <https://github.com/ryankurte/rust-radio-hal> or <https://github.com/japaric/jnet>
- [ ] Rust idiomatic wrapper
